require "rails_helper"

RSpec.describe Emp, type: :model do
  #pending "add some examples to (or delete) #{__FILE__}"
  context "association of Employees" do
    #test_user = Emp.create(empid: "E02", email: "dineshmbps@gmail.com", password: "123456", password_confirmation: "123456")
    it { should have_many :challenges }
    it { should have_many :votes }
    it { should have_many :collaborations }
  end
  context "Validation of Employee" do
    # it { should validate_uniqueness_of :empid }
    it { should validate_presence_of(:empid) }
  end
  context "creation of employee" do
    it "should create user" do
      test_user = Emp.create(empid: "E02", email: "dineshmbps1@gmail.com", password: "123456", password_confirmation: "123456")
      expect(test_user).to be_valid
      test_user.destroy
    end
  end
  context "it should remove employee" do
    it "should destroy user" do
      test_user = Emp.create(empid: "E02", email: "dineshmbps1@gmail.com", password: "123456", password_confirmation: "123456")
      res = "not deleted"
      if test_user.destroy
        res = "deleted"
      end
      expect(res).to eq "deleted"
    end
  end
end
