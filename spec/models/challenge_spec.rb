require "rails_helper"

RSpec.describe Challenge, type: :model do
  #pending "add some examples to (or delete) #{__FILE__}"
  before(:each) do
    @current_user = Emp.create(empid: "E02", email: "dineshmbps1@gmail.com", password: "123456", password_confirmation: "123456")
  end
  after(:each) do
    @current_user.destroy
  end
  context "association of the challenges" do
    it { should belong_to(:emp) }
    it { should have_many(:collaborations) }
    it { should have_many(:tags) }
    it { should have_many(:votes) }
  end
  context "validations of challenges" do
    it { should validate_presence_of(:title) }
    it { should validate_presence_of(:description) }
  end
  context "check only authenticated user can add challenge" do
    it "should be invalid if post is being added and user does not exist" do
      challenge = Challenge.new(title: "Test training", description: "some test description")
      expect(challenge).to_not be_valid
    end
  end
  context "Adding and removing the Challenges" do
    it "should create new Challenge" do
      #test_user = Emp.create(empid: "E02", email: "dineshmbps1@gmail.com", password: "123456", password_confirmation: "123456")
      sample_challenge = @current_user.challenges.build(title: "challenge_title", description: "challenge description")
      expect(sample_challenge).to be_valid
      #@current_user.destroy
    end
    it "should delete the challenge" do
      #@current_user = Emp.create(empid: "E02", email: "dineshmbps1@gmail.com", password: "123456", password_confirmation: "123456")
      sample_challenge = @current_user.challenges.build(title: "challenge_title", description: "challenge description")
      #test_user.destroy
      sample_challenge.save
      challenge_to_be_deleted = @current_user.challenges.last
      res = true
      if challenge_to_be_deleted.destroy
        res = false
      end
      expect(res).to eq false
      #@current_user.destroy
    end
  end
end
