module VotesHelper
  class VoteFactory < BaseFactory
    def adder(params, current_emp)
      @vote = VoteAdder.new(params).addvote(current_emp)
    end
  end
end
