module ChallengesHelper
  class ChallengeFactory < BaseFactory
    def adder(challenge_params, current_emp)
      ChallengeCreator.new(challenge_params).creation(current_emp)
    end
  end
end
