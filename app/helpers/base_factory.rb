class BaseFactory
  def adder
    raise NotImplementationError, "Method Declared in subclass"
  end
end
