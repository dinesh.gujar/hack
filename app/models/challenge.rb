class Challenge < ApplicationRecord
  belongs_to :emp
  has_many :tags, dependent: :destroy
  has_many :votes, dependent: :destroy
  has_many :collaborations, dependent: :destroy

  validates :title, presence: true
  validates :description, presence: true

  def is_liked(emp)
    Vote.find_by(emp_id: emp.id, challenge_id: id)
  end

  def is_collaborated(emp)
    Collaboration.find_by(emp_id: emp.id, challenge_id: id)
  end

  def self.idsearch(search)
    if search
      @challenges = Challenge.where("emp_id like '%#{search}%'")
      #if challenge_of
      #  self.where(challenge_id: challenge_of)
      #else
      #  @challenges = Challenge.all
      #end
    else
      @challenges = Challenge.all
    end
  end
  def self.title_search(search)
    if search
      @challenges = Challenge.where("title like '%#{search}'")
    else
      @challenges = Challenge.all
    end
  end
end
