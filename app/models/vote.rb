class Vote < ApplicationRecord
  belongs_to :challenge
  belongs_to :emp

  validates :emp_id, uniqueness: {scope: :challenge_id}
end
