class ChallengesIndex < ChallengeBase
  include ChallengesChain

  def initialize(params)
    #@params = params
    #@challenges = Challenge.all.includes(:votes, :tags, :collaborations)
    super
  end

  def display_challenges
    c = Client.new
    idsearch = IdSearch.new
    title = TitleSearch.new
    sortvote = SortByVotes.new
    sorttitle = SortByTitle.new
    idsearch.next_handler(title).next_handler(sortvote).next_handler(sorttitle)
    chal = c.display(idsearch, @params)
    if chal
      @challenges = chal
    else
      @challenges = @challenges.all.order(@params[:sort]).reverse_order
    end
  end
end
=begin
if @params[:idsearch] != nil
  @challenges = Challenge.idsearch(@params[:idsearch])
elsif @params[:title_search] != nil
  @challenges = Challenges.title_search(@params[:title_search])
elsif @params[:sort_by] == "top_votes"
  @challenges = @challenges.sort_by { |challenge| challenge.votes.count }.reverse
elsif @params[:sort] == "title"
  @challenges = @challenges.all.order(@params[:sort])
else
  @challenges = @challenges.all.order(@params[:sort]).reverse_order
end
=end
