class ChallengeBase
  def initialize(params)
    @params = params
    @challenges = Challenge.all.includes(:votes, :tags, :collaborations)
  end
end
