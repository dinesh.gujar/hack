class VoteAdder
  def initialize(params)
    @params = params
  end

  def addvote(current_emp)
    @vote = current_emp.votes.build(vote_params)
    return @vote
  end

  private

  def vote_params
    @params.permit :challenge_id
  end
end
