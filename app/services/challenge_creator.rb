class ChallengeCreator < ChallengeBase
  def initialize(params)
    #@params = params
    #@challenge = Challenge
    super
  end

  def creation(current_emp)
    @challenge = current_emp.challenges.build(title: @params[:title], description: @params[:description])
    if @challenge.save
      tag_string = @params[:tag]
      tag_array = tag_string.split(",")
      tag_array.each do |tag_name|
        @challenge.tags.create(name: tag_name)
      end
      return @challenge
    end
  end
end
