module ChallengesChain
  class Handler
    def next_handler=(handler)
      raise NotImplementationError, "#{self.class} Not implemented in this class"
    end
  end

  class AbstractHandler < Handler
    def next_handler(handler)
      @next_handler = handler
    end

    def handle(request)
      if @next_handler
        return @next_handler.handle(request)
      end
      nil
    end
  end

  class IdSearch < AbstractHandler
    def handle(params)
      if params[:idsearch] != nil
        @challenges = Challenge.idsearch(@params[:idsearch])
      else
        super
      end
    end
  end

  class TitleSearch < AbstractHandler
    def handle(params)
      if params[:title_search] != nil
        @challenges = Challenge.title_search(@params[:title_search])
      else
        super
      end
    end
  end

  class SortByVotes < AbstractHandler
    def handle(params)
      if params[:sort_by] == "top_votes"
        @challenges = Challenge.all
        @challenges = @challenges.sort_by { |challenge| challenge.votes.count }.reverse
      else
        super
      end
    end
  end

  class SortByTitle < AbstractHandler
    def handle(params)
      if params[:sort] == "title"
        @challenges = Challenge.all.order(params[:sort])
      else
        super
      end
    end
  end

  class Client
    def display(handler, params)
      result = handler.handle(params)
      if result
        result
      else
        nil
      end
    end
  end
end
=begin
def display_challenges
  if @params[:idsearch] != nil
    @challenges = Challenge.idsearch(@params[:idsearch])
  elsif @params[:title_search] != nil
    @challenges = Challenges.title_search(@params[:title_search])
  elsif @params[:sort_by] == "top_votes"
    @challenges = @challenges.sort_by { |challenge| challenge.votes.count }.reverse
  elsif @params[:sort] == "title"
    @challenges = @challenges.all.order(@params[:sort])
  else
    @challenges = @challenges.all.order(@params[:sort]).reverse_order
  end
end
=end
