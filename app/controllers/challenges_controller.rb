class ChallengesController < ApplicationController
  include ChallengesHelper
  before_action :set_challenge, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_emp!, only: [:edit, :update, :destroy]
  # GET /challenges
  # GET /challenges.json
  def index
    @challenges = ChallengesIndex.new(params).display_challenges
  end

  # GET /challenges/1
  # GET /challenges/1.json
  def show
  end

  # GET /challenges/new
  def new
    @challenge = Challenge.new
  end

  # GET /challenges/1/edit

  # POST /challenges
  # POST /challenges.json
  def create
    #@challenge = Challenge.new(challenge_params)
    #@challenge = current_emp.challenges.build(title: params[:challenge][:title], description: params[:challenge][:description])
    #@challenge = ChallengeCreator.new(challenge_params).creation(current_emp)
    @challenge = ChallengeFactory.new.challenge_creator(challenge_params, current_emp)
    if @challenge
      respond_to do |format|
        format.html { redirect_to root_path, notice: "Challenge was successfully created." }
        format.json { render :show, status: :created, location: @challenge }
      end
    else
      respond_to do |format|
        format.html { render :new }
        format.json { render json: @challenge.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /challenges/1
  # DELETE /challenges/1.json
  def destroy
    @challenge.destroy
    respond_to do |format|
      format.html { redirect_to challenges_url, notice: "Challenge was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_challenge
    @challenge = Challenge.find(params[:id])
    #  @challenges = Challenge.search(params[:search])
  end

  # Only allow a list of trusted parameters through.
  def challenge_params
    params.require(:challenge).permit(:title, :description, :emp_id, :tag, :idsearch, :title_search, :current_emp, :sort_by)
  end
end

#index
#@challenges = Challenge.all

#elsif params[:current_emp] != nil
# @challenges = Challenge.get_challenges(params[:current_emp])

#@challenges = Challenge.all.order(created_at: :desc)
#@challenges = Challenge.includes(:votes).group("challenge_id").order(count: :desc).count
#@challenges = Challenge.joins(:votes).group("challenge_id").order(challenge_id: :desc)
=begin
  def edit
  end

  def update
    respond_to do |format|
      if @challenge.update(challenge_params)
        format.html { redirect_to @challenge, notice: "Challenge was successfully updated." }
        format.json { render :show, status: :ok, location: @challenge }
      else
        format.html { render :edit }
        format.json { render json: @challenge.errors, status: :unprocessable_entity }
      end
    end
  end
=end
