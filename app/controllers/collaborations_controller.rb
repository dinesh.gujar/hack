class CollaborationsController < ApplicationController
  before_action :authenticate_emp!

  def create
    @collaboration = current_emp.collaborations.build(collaboration_params)
    puts @collaboration
    @challenge = @collaboration.challenge
    if @challenge.emp.id != current_emp.id
      if @collaboration.save
        #flash[:notice]="request for collaboration sent"
        redirect_to root_path, notice: "collaboration sent"
      else
        #   flash[:notice]="something went wrong"
        redirect_to root_path, notice: "Something went wrong"
      end
    else
      redirect_to root_path, notice: "Nah Buddy you cant collaborate with your own self"
    end
  end

  def destroy
    @collaboration = Collaboration.find(params[:id])
    @post = @collaboration.challenge
    if @collaboration.destroy
      redirect_to root_path, notice: "Ohh successfully backed off from collaboration"
    else
      redirect_to root_path, notice: "Something went wrong"
    end
  end

  private

  def collaboration_params
    params.permit :challenge_id
  end
end
