#load "vote_adding_service"

class VotesController < ApplicationController
  before_action :authenticate_emp!

  include VotesHelper

  def create
    #@vote = current_emp.votes.build(vote_params)
    #@vote = VoteAdder.new(params).addvote(current_emp)
    @vote = VoteFactory.new.adder(params, current_emp)
    @challenge = @vote.challenge
    if @challenge.emp.id != current_emp.id
      if @vote.save
        #flash[:notice]="voted"
        redirect_to root_path notice: "Thanks for voting"
      else
        #flash[:notice] = "something went wrong"
        redirect_to root_path notice: "Something went wrong"
      end
    else
      redirect_to root_path, notice: "Nah Buddy u cant Upvote your own challenge"
      #redirect_to challenge_path, :notice = "Nah Buddy u cant Upvote your own challenge"
    end
  end

  def destroy
    @vote = Vote.find(params[:id])
    @post = @vote.challenge
    if @vote.destroy
      #flash[:notice]="vote deleted"
      redirect_to root_path notice: "Ohh successfully removed your vote"
    else
      #flash[:notice]= "Somethign went wrong"
      redirect_to root_path notice: "Something went wrong"
    end
  end

  private

  def vote_params
    params.permit :challenge_id
  end
end
