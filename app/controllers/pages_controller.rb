class PagesController < ApplicationController
  def index
    @challenges = Challenge.joins(:votes).group("id").order(id: :desc)
  end
end
